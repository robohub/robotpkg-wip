# robotpkg Makefile for:	motion/pbwbc
# Created:			Florent Lamiraux on Fri, 8 Jul 2011
#

PKGNAME=		pbwbc-${VERSION}
DISTNAME=		pbwbc-${VERSION}
VERSION=		0.1.1
PKGREVISION=		0

MASTER_SITES=		https://gitlab.com/robohub/pbwbc/-/archive/v0.1.1/pbwbc-v0.1.1.tar.gz
#MASTER_REPOSITORY=	${MASTER_REPOSITORY_GITLAB}/robohub/pbwbc
MASTER_REPOSITORY=	git git@gitlab.com:robohub/pbwbc.git

MAINTAINER=	pbwbc@alexanderwerner.net
CATEGORIES=	motion
COMMENT=	An implementation of a passivity based whole-body control framework.
LICENSE=	2-clause-bsd

WRKSRC=		${WRKDIR}/${DISTNAME:a=}

DYNAMIC_PLIST_DIRS+=	share/doc/pbwbc

CMAKE_ARGS+=		-DPYTHON_EXECUTABLE=${PYTHON}
CMAKE_ARGS+=		-DPYTHON_INCLUDE_DIR=${PYTHON_INCLUDE}
CMAKE_ARGS+=		-DPYTHON_LIBRARY=${PYTHON_LIB}
CMAKE_ARGS+=		-DPYTHON_SITELIB=${PYTHON_SITELIB}
CMAKE_ARGS+=		-DSPHINX_BUILD=${SPHINX_BUILD}
CMAKE_ARGS+=		-DCMAKE_INSTALL_LIBDIR=lib

# Build the package in a separate directory
CONFIGURE_DIRS=		_build
CMAKE_ARG_PATH=		..

include ../../meta-pkgs/ros-base/Makefile.common

include ../../devel/boost-headers/depend.mk
include ../../devel/boost-libs/depend.mk
include ../../devel/ros-catkin/depend.mk
include ../../devel/ros-cmake-modules/depend.mk
include ../../devel/ros-pluginlib/depend.mk
include ../../devel/ros-realtime-tools/depend.mk
include ../../motion/ros-control/depend.mk
include ../../pkgtools/pkg-config/depend.mk
include ../../mk/sysdep/cmake.mk
include ../../mk/sysdep/doxygen.mk
include ../../mk/sysdep/graphviz.mk
include ../../mk/sysdep/python.mk
include ../../mk/language/c.mk
include ../../mk/language/c++.mk
include ../../devel/tinyxml/depend.mk
include ../../interfaces/ros-std-msgs/depend.mk
include ../../math/ros-geometry/depend.mk
include ../../math/pinocchio/depend.mk
include ../../motion/ros-control-toolbox/depend.mk
include ../../lang/ros-message-generation/depend.mk
include ../../lang/ros-message-runtime/depend.mk
include ../../middleware/ros-comm/depend.mk
include ../../middleware/ros-dynamic-reconfigure/depend.mk
include ../../wip/ddynamic-reconfigure-python/depend.mk
include ../../net/ros-resource-retriever/depend.mk
include ../../mk/robotpkg.mk

pre-configure:
	${RUN}${MKDIR} ${WRKSRC}/_build
