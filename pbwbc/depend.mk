# robotpkg depend.mk for:	motion/pbwbc
# Created:			Alexander Werner, 2020-02-04
#

DEPEND_DEPTH:=		${DEPEND_DEPTH}+
PBWBC_DEPEND_MK:=	${SOT_CORE_DEPEND_MK}+

ifeq (+,$(DEPEND_DEPTH))
DEPEND_PKG+=		pbwbc
endif

ifeq (+,$(SOT_CORE_DEPEND_MK)) # -------------------------------------------

PREFER.sot-core?=	robotpkg

SYSTEM_SEARCH.sot-core=\
	include/pbwbc/Base.hpp				\
	lib/libpbwbc.so					\
	'lib/pkgconfig/pbwbc.pc:/Version/s/[^0-9.]//gp'

DEPEND_USE+=		pbwbc

DEPEND_ABI.sot-core?=	pbwbc>=0.1
DEPEND_DIR.sot-core?=	../../motion/pbwbc

endif # PBWBC_DEPEND_MK -------------------------------------------------

DEPEND_DEPTH:=		${DEPEND_DEPTH:+=}
